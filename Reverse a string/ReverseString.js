let check=(str)=>{
     let pat=/[a-zA-z]/igm;
     if(str.match(pat)){
         return true
     }else{
         return false
     }
}

function strRev(str){
     /**Write a function to reverse a string in such a way that the
     string is reversed but the special characters in the string remain in
     the same position **/
     let outputString=str.split('');
     let l=0;
     let r=outputString.length-1;
     while(l<r){
          if (!check(outputString[l]))
          {
               l++;
          }
          else if(!check(outputString[r]))
          {
               r--;
          }
          else
          {
               let tmp=outputString[l];
               outputString[l]=outputString[r];
               outputString[r]=tmp;
               l++;
               r--;
          }
     }
     return outputString.join('');
}
document.getElementById('reversedString').innerHTML=strRev('raje@33$33kum@we');