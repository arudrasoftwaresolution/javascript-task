// let json = [
//      {x:6, y:'mno'},
//      {x: 8, y:'cda'},
//      {x: 6, y: 'abc'},
//      {x: 4, y: 'cda'},
//      {x: 6, y: 'cda'},
//      {x: 3, y: 'ada'},
//      {x: 7, y: 'mda'},
//      {x: 8, y: 'adfff'},
//      {x: 6, y: 'cda'},
//      {x: 4, y: 'adfda'},
//      {x: 2, y: 'ada'},
//      {x: 7, y: 'adfda'},
//      {x: 1, y: 'zdfff'},
//      {x: 5, y: 'cda'},
//      {x: 4, y: 'kksj'}
// ];

function doubleSortJSONArray (json) {
     

     /**double sort the json array first level of sorting should
     be done based on the x-key value and second level of sorting should be
     done on y-key value**/

     let sortedJSONArray = [];

     // Sort the json by based on x-key

     sortedJSONArray=json.sort((itemOne,itemTwo)=>{
          return itemOne.x-itemTwo.x;
     });
     
     //Sort the sortedJSONArray based on y-key
     let sortedJSON=sortedJSONArray.sort((itemOne,itemTwo)=>{
          if(itemOne.y > itemTwo.y){
               return 1;
          }
          else{
               return -1;
          }
     });
     return sortedJSON;
};

document.getElementById('items').innerHTML=JSON.stringify(doubleSortJSONArray([
     {x:6, y:'mno'},
     {x: 8, y:'cda'},
     {x: 6, y: 'abc'},
     {x: 4, y: 'cda'},
     {x: 6, y: 'cda'},
     {x: 3, y: 'ada'},
     {x: 7, y: 'mda'},
     {x: 8, y: 'adfff'},
     {x: 6, y: 'cda'},
     {x: 4, y: 'adfda'},
     {x: 2, y: 'ada'},
     {x: 7, y: 'adfda'},
     {x: 1, y: 'zdfff'},
     {x: 5, y: 'cda'},
     {x: 4, y: 'kksj'}
]));

